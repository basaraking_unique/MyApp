package com.example.administrator.myapp;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.administrator.myapp.bean.Result;
import com.example.administrator.myapp.bean.User;
import com.example.administrator.myapp.http.LocalHttpUtils;
import com.example.administrator.myapp.http.MyCallBack;
import com.example.administrator.utils.JsonUtils;
import com.example.administrator.utils.app.AppContent;
import com.example.administrator.utils.app.BaseActivity;
import com.lzy.okgo.model.Response;

import org.w3c.dom.Text;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private EditText et_ssid, et_pwd;
    private Button bt_login;
    private TextView tv_reg;
    private ImageView iv;

    @Override
    protected int getLayoutId() {
        return R.layout.login_act;
    }

    @Override
    protected void initView() {
        et_ssid = findViewById(R.id.et_ssid);
        et_pwd = findViewById(R.id.et_pwd);
        bt_login = findViewById(R.id.bt_login);
        tv_reg = findViewById(R.id.tv_reg);
        iv=findViewById(R.id.iv);
    }

    @Override
    protected void initData() {
        String img_url="http://img.sccnn.com/bimg/338/24556.jpg";
        Glide.with(this)
                .load(img_url)
                .into(iv);
    }

    @Override
    protected void initListener() {
        bt_login.setOnClickListener(this);
        tv_reg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == bt_login) {
            String ssid = et_ssid.getText().toString();
            String pwd = et_pwd.getText().toString();
            if(TextUtils.isEmpty(ssid)|| TextUtils.isEmpty(pwd)){
                showToast("您的账号或密码不符合要求");
                return;
            }
            LocalHttpUtils.login(this, ssid, pwd, callBack);
        } else if (view == tv_reg) {
            jumpToActivity(RegActivity.class);
        }
    }

    private MyCallBack callBack = new MyCallBack() {
        @Override
        public void onStart(Object tag) {
            super.onStart(tag);
            showDialog();
        }

        @Override
        public void onSuccess(String json) {
            super.onSuccess(json);
            Result<User> result = JsonUtils.toBean(json, JsonUtils.newParamType(Result.class, User.class));
            switch (result.getCode()) {
                case 1: {
                    saveSP(AppContent.SP_USER_TOKEN,result.getData().getToken());
                    jumpToActivity(MainActivity.class);
                    finish();
                    break;
                }
                case -1: {
                    showToast("账号或密码错误");
                    break;
                }
            }
        }

        @Override
        public void onError(Response<String> response) {
            super.onError(response);
            showToast("网络异常，请重试");
        }

        @Override
        public void onFinish() {
            super.onFinish();
            dismissDialog();
        }
    };
}
