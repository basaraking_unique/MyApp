package com.example.administrator.myapp.http;

import com.example.administrator.utils.LogUtils;
import com.example.administrator.utils.http.JsonCallback;

public class MyCallBack extends JsonCallback {
    @Override
    public void onStart(Object tag) {

    }

    @Override
    public void onSuccess(String json) {

    }

    @Override
    public void onError(Throwable exception) {
        LogUtils.e(exception.getMessage());
        exception.printStackTrace();
    }

    @Override
    public void onFinish() {

    }
}
