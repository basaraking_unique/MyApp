package com.example.administrator.myapp;

import android.app.Application;

import com.example.administrator.utils.http.HttpUtils;

public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        HttpUtils.init(this);
    }
}
