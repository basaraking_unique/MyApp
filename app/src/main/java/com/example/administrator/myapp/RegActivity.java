package com.example.administrator.myapp;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.administrator.myapp.bean.Result;
import com.example.administrator.myapp.bean.User;
import com.example.administrator.myapp.http.LocalHttpUtils;
import com.example.administrator.myapp.http.MyCallBack;
import com.example.administrator.utils.JsonUtils;
import com.example.administrator.utils.app.AppContent;
import com.example.administrator.utils.app.BaseActivity;
import com.example.administrator.utils.http.HttpUtils;
import com.lzy.okgo.model.Response;

public class RegActivity extends BaseActivity implements View.OnClickListener {

    private EditText et_ssid1, et_pwd1, et_pwd2, et_name;
    private Button bt_reg;

    @Override
    protected int getLayoutId() {
        return R.layout.reg_act;
    }

    @Override
    protected void initView() {
        et_ssid1 = findViewById(R.id.et_ssid1);
        et_pwd1 = findViewById(R.id.et_pwd1);
        et_pwd2 = findViewById(R.id.et_pwd2);
        et_name = findViewById(R.id.et_name);
        bt_reg = findViewById(R.id.bt_reg);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void initListener() {
        bt_reg.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == bt_reg) {
            String ssid=et_ssid1.getText().toString();
            String pwd1=et_pwd1.getText().toString();
            String pwd2=et_pwd2.getText().toString();
            String name=et_name.getText().toString();
            if(TextUtils.isEmpty(ssid)||TextUtils.isEmpty(pwd1)||TextUtils.isEmpty(pwd2)||TextUtils.isEmpty(name)||!pwd1.equals(pwd2)){
                showToast("信息输入为空或两次密码输入不同");
                return;
            }
            LocalHttpUtils.reg(this,ssid,pwd1,name,callBack);
        }
    }

    private MyCallBack callBack = new MyCallBack() {
        @Override
        public void onStart(Object tag) {
            super.onStart(tag);
            showDialog();
        }

        @Override
        public void onSuccess(String json) {
            super.onSuccess(json);
            Result<User> result = JsonUtils.toBean(json, JsonUtils.newParamType(Result.class, User.class));
            switch (result.getCode()) {
                case 1: {
                    finish();
                    break;
                }
                case -1: {
                    showToast("输入信息错误错误");
                    break;
                }
            }
        }

        @Override
        public void onError(Response<String> response) {
            super.onError(response);
            showToast("网络异常，请重试");
        }

        @Override
        public void onFinish() {
            super.onFinish();
            dismissDialog();
        }
    };
}
