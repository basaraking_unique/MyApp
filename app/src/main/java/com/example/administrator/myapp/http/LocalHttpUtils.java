package com.example.administrator.myapp.http;

import com.example.administrator.utils.http.HttpUtils;
import com.example.administrator.utils.http.JsonCallback;

import java.util.HashMap;
import java.util.Map;

public class LocalHttpUtils extends HttpUtils {
    public static final String base_url="http://192.168.10.61:8080/WebLoginTest";
    public static void login(Object tag, String ssid, String pwd, JsonCallback callback){
        String url=base_url+"/login";
        Map<String,String> params=new HashMap<>();
        params.put("ssid",ssid);
        params.put("password",pwd);
        HttpUtils.post(tag,url,params,callback);
    }
    public static void reg(Object tag, String ssid, String pwd, String name,JsonCallback callback){
        String url=base_url+"/reg";
        Map<String,String> params=new HashMap<>();
        params.put("ssid",ssid);
        params.put("password",pwd);
        params.put("name",name);
        HttpUtils.post(tag,url,params,callback);
    }
}
